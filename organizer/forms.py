from django import forms
from .models import Tag,NewsLink,Startup
from django.core.exceptions import ValidationError
from django.forms.widgets import HiddenInput


class SlugCleanMixin:
    def clean_slug(self):
        new_slug=self.cleaned_data['slug'].lower()
        if new_slug=='create':
            raise ValidationError('Slug may not be create')
        return new_slug


class NewsLinkForm(SlugCleanMixin, forms.ModelForm):
    class Meta:
        model=NewsLink
        fields= '__all__'
        widgets={'startup':HiddenInput()}

 #       exclude=('startup',)

    # def save(self,**kwargs):
    #     startup_obj=kwargs.get('startup_obj',None)
    #     if startup_obj is not None:
    #         instance=super().save(commit=False)
    #         instance.startup=startup_obj
    #         instance.save()
    #         self.save_m2m()
    #     else:
    #         instance=super().save()
    #     return instance
    # def clean(self):
    #     cleaned_data=super().clean()
    #     slug=cleaned_data.get('slug')
    #     startup_obj=self.data.get('startup')
    #     exists=(
    #         NewsLink.objects.filter(
    #             slug__iexact=slug,
    #             startup=startup_obj,
    #         ).exists())
    #     if exists:
    #         raise ValidationError(
    #             "News articles with this Slug and Startup"
    #             " already exists.")
    #     else:
    #         instance=super().save()
    #         return cleaned_data
    # def save(self,**kwargs):
    #     instance=super().save(commit=False)
    #     instance.startup=(
    #         self.data.get('startup'))
    #     instance.save()
    #     self.save_m2m()
    #     return instance

class StartupForm(SlugCleanMixin,forms.ModelForm):
    class Meta:
        model=Startup
        fields='__all__'


class TagForm(SlugCleanMixin,forms.ModelForm):
    class Meta:
        model=Tag
        fields=['name','slug']

    def clean_name(self):
        return self.cleaned_data['name'].lower()
    def save(self):
        new_tag=Tag.objects.create(
            name=self.cleaned_data['name'],
            slug=self.cleaned_data['slug'])
        return new_tag