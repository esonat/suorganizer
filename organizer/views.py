from django.shortcuts import render
from .models import Startup,Tag,NewsLink
from django.shortcuts import get_object_or_404,redirect,render_to_response,render
from .forms import TagForm,StartupForm,NewsLinkForm
from django.views.generic import \
    (CreateView,DeleteView,
     DetailView,View,ListView)
from django.core.urlresolvers import reverse,reverse_lazy
from django.core.paginator import (
    EmptyPage,PageNotAnInteger,Paginator)
from core.utils import UpdateView
from .utils import PageLinksMixin,\
    StartupContextMixin,NewsLinkGetObjectMixin
from django.contrib.auth.decorators import login_required,\
    permission_required,user_passes_test
from django.utils.decorators import method_decorator
from django.contrib.auth import PermissionDenied
from bloguser.decorators import \
    class_login_required,require_authenticated_permission

@require_authenticated_permission(
    'organizer.delete_newslink')
class NewsLinkDelete(StartupContextMixin,
                     DeleteView):
    model=NewsLink
    slug_url_kwarg='newslink_slug'

    def get_success_url(self):
        return (self.object.startup.get_absolute_url())

@require_authenticated_permission(
    'organizer.change_newslink')
class NewsLinkUpdate(NewsLinkGetObjectMixin,
                     StartupContextMixin,
                     UpdateView):
    form_class=NewsLinkForm
    model=NewsLink
    slug_url_kwarg='newslink_slug'

@require_authenticated_permission(
    'organizer.add_newslink')
class NewsLinkCreate(NewsLinkGetObjectMixin,
                     StartupContextMixin,
                     CreateView):
    form_class=NewsLinkForm
    model=NewsLink

    def get_initial(self):
        startup_slug=self.kwargs.get(
            self.startup_slug_url_kwarg)
        self.startup=get_object_or_404(
            Startup,slug__iexact=startup_slug)
        initial={
            self.startup_context_object_name:
                self.startup,
        }
        initial.update(self.initial)
        return initial

@require_authenticated_permission(
    'organizer.add_startup')
class StartupCreate(CreateView):
    form_class=StartupForm
    model=Startup


class StartupDetail(DetailView):
    model=Startup

class StartupList(PageLinksMixin,ListView):
    model=Startup
    paginate_by=5

@require_authenticated_permission(
    'organizer.change_startup')
class StartupUpdate(UpdateView):
    form_class = StartupForm
    model=Startup

@require_authenticated_permission(
    'organizer.delete_startup')
class StartupDelete(DeleteView):
    model=Startup
    success_url = reverse_lazy('organizer_startup_list')

def startup_list(request):
    return render(
        request,
        'organizer/startup_list.html',
        {'startup_list':Startup.objects.all()})


class TagDetail(DetailView):
    model=Tag


class TagList(PageLinksMixin,ListView):
    paginate_by = 5
    model=Tag

def in_contrib_group(user):
    if user.groups.filter(
        name='contributors').exists():
        return True
    else:
        raise PermissionDenied

@require_authenticated_permission(
    'organizer.add_tag')
class TagCreate(CreateView):
    form_class=TagForm
    model=Tag

@require_authenticated_permission(
    'organizer.change_tag')
class TagUpdate(UpdateView):
    form_class=TagForm
    model=Tag

@require_authenticated_permission(
    'organizer.delete_tag')
class TagDelete(DeleteView):
    model=Tag
    success_url = reverse_lazy('organizer_tag_list')

def tag_create(request):
    if request.method=='POST':
        form=TagForm(request.POST)
        if form.is_valid():
            new_tag=form.save()
            return redirect(new_tag)
    else:
        form=TagForm()
    return render(
        request,
        'organizer/tag_form.html',
        {'form':form})


def model_list(request,model):
    context_object_name='{}_list'.format(model._meta.model_name)
    context={
        context_object_name:model.objects.all(),
    }
    template_name=(
        'organizer/{}_list.html'.format(
            model._meta.model_name))

    return render(request,template_name,context)
