from django.conf.urls import url
from ..views import (
    TagCreate, TagDelete, TagUpdate, TagDetail,
    TagList,model_list)
from ..models import Tag
from django.views.generic import DetailView
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$',
        TagList.as_view(),
        #{'model':Tag},
        name='organizer_tag_list'),
    url(r'^create/$',
        TagCreate.as_view(),
        name='organizer_tag_create'),
    url(r'^(?P<slug>[\w\-]+)/$',
        TagDetail.as_view(),
        # DetailView.as_view(
        #     context_object_name='tag',
        #     model=Tag,
        #     template_name=('organizer/tag_detail.html')),
        name='organizer_tag_detail'),
    url(r'^(?P<slug>[\w-]+)/delete/$',
        TagDelete.as_view(),
        name='organizer_tag_delete'),
    url(r'^(?P<slug>[\w\-]+)/update/$',
        TagUpdate.as_view(),
        name='organizer_tag_update'),
]
