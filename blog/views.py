from django.shortcuts import render
from django.shortcuts import (get_object_or_404,render)
from .models import Post
from django.views.generic import CreateView,ListView,\
    YearArchiveView,View,\
    MonthArchiveView,ArchiveIndexView,\
    DateDetailView,DetailView,DeleteView
from django.views.decorators.http import require_http_methods
from .forms import PostForm
from django.shortcuts import get_object_or_404,render,redirect
from .utils import DateObjectMixin
from core.utils import UpdateView
from django.core.urlresolvers import reverse,reverse_lazy
from bloguser.decorators import require_authenticated_permission
from .utils import AllowFuturePermissionsMixin,DateObjectMixin

@require_authenticated_permission(
    'blog.add_post')
class PostCreate(CreateView):
    form_class=PostForm
    model=Post


class PostDetail(DateObjectMixin,DetailView):
   # allow_future=True
    date_field = 'pub_date'
    model=Post


class PostArchiveMonth(
        AllowFuturePermissionsMixin,
        MonthArchiveView):
    model=Post
    date_field = 'pub_date'
    month_format = '%m'

class PostArchiveYear(
        AllowFuturePermissionsMixin,
        YearArchiveView):
    model=Post
    date_field = 'pub_date'
    make_object_list = True

@require_authenticated_permission(
    'blog.change_post')
class PostUpdate(DateObjectMixin,UpdateView):
    #allow_future = True
    date_field = 'pub_date'
    form_class=PostForm
    model=Post

@require_authenticated_permission(
    'blog.delete_post')
class PostDelete(DateObjectMixin,DeleteView):
    # allow_future = True
    date_field = 'pub_date'
    model=Post
    success_url = reverse_lazy('blog_post_list')


class PostList(
        AllowFuturePermissionsMixin,
        ArchiveIndexView):
    allow_empty = True
    #allow_future = True
    context_object_name = 'post_list'
    date_field = 'pub_date'
    make_object_list=True
    model=Post
    paginate_by = 5
    template_name = 'blog/post_list.html'

def post_list(request):
    return render(
        request,
        'blog/post_list.html',
        {'post_list':Post.objects.all()})
